module gitlab.com/restbeast/cli

go 1.13

require (
	github.com/agext/levenshtein v1.2.3 // indirect
	github.com/brianvoe/gofakeit/v5 v5.9.0
	github.com/getsentry/sentry-go v0.7.0
	github.com/go-errors/errors v1.1.1
	github.com/google/go-cmp v0.5.0 // indirect
	github.com/hashicorp/hcl/v2 v2.6.0
	github.com/mitchellh/go-wordwrap v1.0.0 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/zclconf/go-cty v1.5.1
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	golang.org/x/sys v0.0.0-20200720211630-cb9d2d5c5666 // indirect
	golang.org/x/text v0.3.3 // indirect
)
